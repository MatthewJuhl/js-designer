!function (w, d, undefined)
{
	'use strict';
	// TODO: allow embedding woff fonts with a file input (base64 encode them)
		// https://developer.mozilla.org/en-US/docs/Web/API/FileReader/readAsDataURL
	// TODO: shapes should be automatically assigned names, eg 'Line 1'
	var SHAPE_CLASS = 'designer-c__shape';
	var TEXT_DRAGGING_CLASS = 'designer-c__text--dragging';
	var TEXT_SHAPE_CLASS = 'designer-c__shape--text';
	var LINE_CLASS = 'designer-c__shape--line';

	// keyCodes:
	var KEYMAP = []; // plain text representation of key
	var KEY_LEFT = 37; KEYMAP[KEY_LEFT] = 'left';
	var KEY_UP = 38; KEYMAP[KEY_UP] = 'up';
	var KEY_RIGHT = 39; KEYMAP[KEY_RIGHT] = 'right';
	var KEY_DOWN = 40; KEYMAP[KEY_DOWN] = 'down';
	var KEY_COMMAND = 91;
	var KEY_CONTROL = 17;
	var KEY_ESC = 27;
	var KEY_SHIFT = 16;
	var KEY_DELETE = 8;
	var KEY_S = 83;
	var KEY_O = 79;
	var KEY_Z = 90;
	var KEY_Y = 89;
	var KEY_V = 86; // move tool uses V
	var KEY_R = 82; // shape tool uses R
	var KEY_T = 84; // text tool
	var KEY_L = 76; // line tool

	// play nicely
	var STORAGE_KEY_PREFIX = 'com.mjuhl.Designer.';
	var DOCUMENT_STORAGE_KEY_PREFIX = STORAGE_KEY_PREFIX + 'document.';

	var selected; // the currently shape
	var current_tool = 'move'; // the currently selected tool
	var page = d.getElementById('designer_contents');
	var wrapper = d.getElementById('designer_contents_wrapper');
	var dragging = false;
	var dragging_rect = {};
	var selected_orig = {}; // x,y
	var editing_text = false;

	var tools_panel = d.getElementById('designer_tools');

	var shape_expando = 'designer' + (new Date).getTime();
	var shape_count = 0;
	var shapes = [];

	var grid_size = 8;


	function init_page ()
	{
		page = d.getElementById('designer_contents');
		// this will (re)initialize all shapes on a page
		// it's typically called when loading a saved document

		// grab all the shapes
		[].forEach.call(page.querySelectorAll('.' + SHAPE_CLASS), function (element)
		{
			var shape = init_shape_from_element(element);
		});
		bind_page_listeners();
	}



	function select (shape)
	{
		// TODO: allow selecting multiple elements!
		// then, always assume selected is an array
		selected = shape;
		selected_orig = {
			x: parseInt(shape.element.style.left),
			y: parseInt(shape.element.style.top)
		};
		console.log('selected:', shape.element);
	}

	function set_tool (tool)
	{
		console.log('tool set: ' + tool);
		current_tool = tool;

		switch (tool) {
			case 'line':
			case 'shape':
				page.style.cursor = 'crosshair';
			break;
			case 'text':
				page.style.cursor = 'text';
			break;
			default:
				page.style.cursor = 'default';
			break;
		}
	}

	function set_grid_size (size)
	{
		grid_size = size;
	}

	function snap_to_grid (val)
	{
		if (!val) return 0;
		var negative = val < 0;
		var val = Math.abs(val);
		var result = val;
		var remainder = val % grid_size;
		var half = grid_size / 2;

		result = Math.floor((val / grid_size) + .5) * grid_size;

		if (negative) result = 0 - result;
		return result;
	}

	function init_drag (e, page)
	{
		dragging = true;
		dragging_rect.startX = e.pageX;
		dragging_rect.startY = e.pageY;
	}

	function continue_drag (e, page)
	{
		dragging_rect.w = (e.pageX - page.offsetLeft) - dragging_rect.startX;
		dragging_rect.h = (e.pageY - page.offsetTop) - dragging_rect.startY;
	}

	function init_text_edit ()
	{
		console.log('enter text edit mode');
		editing_text = true;
		selected.element.setAttribute('contenteditable', true);
	}

	function stop_text_edit ()
	{
		console.log('end text edit mode');
		editing_text = false;
		if (selected) selected.element.setAttribute('contenteditable', false);
	}

	function delete_selected ()
	{
		if (!selected) return;
		if (confirm('There is currently no undo. Are you sure you want to delete this object?'))
		{
			// #domchange
			var id = selected.element[shape_expando];
			selected.element.remove();
			delete shapes[id];
		}
	}

	function camel_to_dashed (string) {
		return string.replace(/\W+/g, '-').replace(/([a-z\d])([A-Z])/g, '$1-$2');
	}

	function set_page_style (prop, value)
	{
		page.style[prop] = value;
	}

	// mouse tools
	var tools =
	{
		shape: // this tool is for drawing new shapes
		{
			mousedown: function (e, page)
			{
				init_drag(e, page);
				// create a new shape at the location of the cursor
				var shape = new_shape(e.pageX, e.pageY);
				select(shape);
			},

			mousemove: function (e, page)
			{
				if (!dragging) return;
				// TODO: allow dragging from the bottom up if the following values are negative
				continue_drag(e, page);
				selected.size(dragging_rect.w, dragging_rect.h);
			},

			mouseup: function (e, page)
			{
				dragging = false;
				set_tool('move');
				if (!selected.element.style.width || !selected.element.style.height)
				{
					// if the user just clicks, the height/width aren't defined
					// so give elements a shape by default
					selected.size(100, 30);
				}
			}
		},

		text: // a tool for creating text shapes
		{
			mousedown: function (e, page)
			{
				init_drag(e, page);
				// create a new shape at the location of the cursor
				var shape = new_shape(e.pageX, e.pageY);
				shape.add_class(TEXT_SHAPE_CLASS);
				// shape.size(260, 60);
				shape.style('backgroundColor', 'transparent');
				shape.style('border', 'none');
				select(shape);
				shape.add_class(TEXT_DRAGGING_CLASS);
			},

			mousemove: function (e, page)
			{
				tools.shape.mousemove(e, page);
			},

			mouseup: function (e, page)
			{
				if (!selected.element.style.width && !selected.element.style.height)
				{
					// if the user just clicks rather than dragging, the height/width aren't defined
					// so give elements a shape by default
					selected.size(160, 30);
				}
				tools.shape.mouseup(e, page);
				selected.remove_class(TEXT_DRAGGING_CLASS);
				selected.element.innerHTML = 'Lorem ipsum dolor sit';
				init_text_edit();
				var range = document.createRange();
				var sel = window.getSelection();
				range.setStart(selected.element, 0);
				range.setEnd(selected.element, 1);
				sel.removeAllRanges();
				sel.addRange(range);
			}
		},

		line: // a tool for creating horizontal lines
		{
			mousedown: function (e, page)
			{
				init_drag(e, page);
				// create a new shape at the location of the cursor
				var shape = new_shape(e.pageX, e.pageY);
				shape.add_class(LINE_CLASS);
				shape.size(1, 1);
				shape.style('border', 'none');
				select(shape);
			},

			mousemove: function (e, page)
			{
				if (!dragging) return;
				// TODO: allow dragging from the bottom up if the following values are negative
				continue_drag(e, page);
				var w = dragging_rect.w;
				var h = dragging_rect.h;
				if (w > h) h = 1;
				else w = 1;
				selected.size(w,h);
			},

			mouseup: function (e, page)
			{
				if (selected.element.style.width == '1px' && selected.element.style.height == '1px')
				{
					// if the user just clicks rather than dragging, the height/width aren't defined
					// so give elements a shape by default
					selected.size(200, 1);
				}
				tools.shape.mouseup(e, page);
			}
		},

		resize:
		{
			// this tool is for changing the size of shapes

		},

		move:
		{
			// this tool is for moving shapes around
			mousedown: function (e, page)
			{

				// allow text selection while editing text
				if (editing_text)
				{
					if (e.target === selected.element)
					{
						console.log('allowing text selection');
						return;
					}
				}

				if (is_element_a_shape(e.target))
				{
					select(get_shape_for_element(e.target));
					init_drag(e, page);
				}
				else
				{
					deselect();
				}
			},

			mousemove: function (e, page)
			{
				if (!dragging) return;
				var snap = e.shiftKey;
				// TODO: this should just translate the element, it should only "move" when dropped
				continue_drag(e, page);
				selected.move(dragging_rect.w, dragging_rect.h, snap);
			},

			mouseup: function (e, page)
			{
				dragging = false;
			},

			keydown_down: function (e, page)
			{
				if (editing_text) return;
				var snap = e.shiftKey;
				if (selected)
				{
					selected.nudge(0, 1, snap);
					e.stopPropagation();
					e.preventDefault();
				}
			},
			keydown_up: function (e, page)
			{
				if (editing_text) return;
				var snap = e.shiftKey;
				if (selected)
				{
					selected.nudge(0, -1, snap);
					e.stopPropagation();
					e.preventDefault();
				}
			},
			keydown_right: function (e, page)
			{
				if (editing_text) return;
				var snap = e.shiftKey;
				if (selected)
				{
					selected.nudge(1, 0, snap);
					e.stopPropagation();
					e.preventDefault();
				}
			},
			keydown_left: function (e, page)
			{
				if (editing_text) return;
				var snap = e.shiftKey;
				if (selected)
				{
					selected.nudge(-1, 0, snap);
					e.stopPropagation();
					e.preventDefault();
				}
			},

			dblclick: function (e, page)
			{
				// ignore this on an element we're already editing so double
				// clicking behaves as you would expect it to (selecting words)
				if (editing_text && selected.element === e.target) return;

				if (is_element_a_text_shape(e.target))
				{
					select(get_shape_for_element(e.target));
					init_text_edit();
					var range = document.createRange();
					var sel = window.getSelection();
					range.setStart(e.target, 1);
					range.collapse(true);
					sel.removeAllRanges();
					sel.addRange(range);
				}
			}
		}
	};

	function deselect ()
	{
		if (editing_text) stop_text_edit();
		if (!selected) return;
		selected.element.blur();
		selected = undefined;
		console.log('all elements deselected');
	}

	function is_element_a_shape (element)
	{
		return element.classList.contains(SHAPE_CLASS);
	}

	function is_element_a_text_shape (element)
	{
		return element.classList.contains(TEXT_SHAPE_CLASS);
	}

	function get_shape_for_element (element)
	{
		var shape_id = element[shape_expando];
		return shapes[shape_id];
	}

	var shape_proto =
	{
		element: null,

		add_class: function (name)
		{
			this.element.classList.add(name);
			return this;
		},

		remove_class: function (name)
		{
			this.element.classList.remove(name);
			return this;
		},

		toggle_class: function (name)
		{
			this.element.classList.toggle(name);
			return this;
		},

		style: function (property, value)
		{
			this.element.style[property] = value;
			return this;
		},

		move: function (left, top, snap) // this is for moving via drag only
		{
			// don't break if passed '5px'
			left = parseInt(left);
			top = parseInt(top);

			var new_left = selected_orig.x + parseInt(left);
			var new_top = selected_orig.y + parseInt(top);

			if (snap)
			{
				new_left = snap_to_grid(new_left);
				new_top = snap_to_grid(new_top);
			}

			// TODO: selected_orig.x should refer to this object somehow
			this.element.style.left = force_px(new_left);
			this.element.style.top = force_px(new_top);
			return this;
		},

		nudge: function (left, top, snap) // this is for moving granularly
		{
			var startleft = parseInt(this.element.style.left);
			var starttop = parseInt(this.element.style.top);
			var addleft = parseInt(left);
			var addtop = parseInt(top);

			if (snap)
			{
				addleft = snap_to_grid(addleft);
				addtop = snap_to_grid(addtop);
			}

			this.element.style.left = force_px(startleft + addleft);
			this.element.style.top = force_px(starttop + addtop);
			return this;
		},

		translate: function (x, y)
		{
			// this isn't quite working
			// TODO: figure out how to easily deal with prefixes (and be future proof)
			this.element.style.webkitTransform = 'translate(' + force_px(x) + ',' + force_px(y) + ')';
		},

		size: function (width, height)
		{
			this.element.style.width = force_px(width);
			this.element.style.height = force_px(height);
			return this;
		}
	};

	function force_px (string_or_value)
	{
		if (!/px$/.test(string_or_value))
		{
			string_or_value += 'px';
		}
		return string_or_value;
	}

	// this is only used by init_page() while loading a saved document
	function init_shape_from_element (element)
	{
		// TODO: merge this with new_shape somehow
		// #duplicatecode
		var shape = Object.create(shape_proto);
		shape.element = element;
		var shape_id = shape_count++;
		shape.element[shape_expando] = shape_id;
		shapes[shape_id] = shape;

		return shape;
	}

	// create a new shape and add it to the page
	function new_shape (left, top)
	{
		// TODO: merge this with init_shape_from_element somehow
		// #duplicatecode
		var shape = Object.create(shape_proto);
		shape.element = document.createElement('div');

		shape.add_class(SHAPE_CLASS);

		var shape_id = shape_count++;
		shape.element[shape_expando] = shape_id;
		shapes[shape_id] = shape;

		shape.style('left', force_px(left || 100));
		shape.style('top', force_px(top || 100));
		// shape.style('width', '10px');
		// shape.style('height', '10px');

		page.appendChild(shape.element);
		return shape;
	}

	function shape_size (shape, width, height)
	{
		shape.style.width = force_px(width);
		shape.style.height = force_px(height);
	}



	function bind_page_listeners ()
	{
		page.addEventListener('mousedown', function (e)
		{
			// ignore left clicks for now
			if (e.button !== 0) return;
			tools[current_tool].mousedown(e, page);
		});

		page.addEventListener('mousemove', function (e)
		{
			tools[current_tool].mousemove(e, page);
		});

		page.addEventListener('mouseup', function (e)
		{
			tools[current_tool].mouseup(e, page);
		});

		page.addEventListener('dblclick', function (e)
		{
			if (tools[current_tool]['dblclick']) tools[current_tool]['dblclick'](e, page);
		});
	}
	bind_page_listeners();


	d.addEventListener('keydown', function (e)
	{
		var code = e.keyCode;
		var method;

		// move keys - no modifiers required
		if (KEYMAP[code])
		{
			// first, look for a method for the current too
			method = tools[current_tool]['keydown_' + KEYMAP[code]];
			if (method) return method(e, page);
		}

		// ESC key always deselects and ends editing mode
		// TODO: If editing, ESC should end editing mode but not deselect.
		if (code === KEY_ESC)
		{
			if (editing_text) stop_text_edit();
			else deselect();
			return;
		}

		// console.log(e);
		// console.log('metaKey', e.metaKey);
		// console.log('ctrlKey', e.ctrlKey);
		// console.log('keyCode', e.keyCode);

		// command+KEY shortcuts work even while editing text
		if (e.metaKey || e.ctrlKey)
		{
			switch (code)
			{
				case KEY_S:
					e.preventDefault(); // stop the native Save dialog
					e.stopPropagation();
					stop_text_edit(); // otherwise, edit mode gets "stuck" on for an element being edited when page is reloaded
					w.designer.save();
					return false;
				break;
				case KEY_O:
					e.preventDefault();
					e.stopPropagation();
					stop_text_edit();

					var document_to_open = prompt('Name of document to open?');
					if (document_to_open)
					{
						w.designer.load(document_to_open);
					}
					return false;
				break;
			}
		}

		// these shortcuts don't work while editing text
		if (editing_text) return;
		switch (code)
		{
			case KEY_V:
				set_tool('move');
			break;
			case KEY_R:
				set_tool('shape');
			break;
			case KEY_T:
				set_tool('text');
			break;
			case KEY_L:
				set_tool('line');
			break;
			case KEY_DELETE:
				delete_selected();
			break;
		}
	});

	tools_panel.addEventListener('mousedown', function (e)
	{
		e.stopPropagation();
	});


	// font embed stuff
	var font_picker_trigger = document.getElementById('designer_add_font_trigger');
	var font_picker_input = document.getElementById('designer_font_picker');
	font_picker_trigger.addEventListener('click', function (e)
	{
		e.stopPropagation();
		font_picker_input.click();
	});
	font_picker_input.addEventListener('change', function (e)
	{
		var font_name = prompt('font family name');
		var font_weight = prompt('font weight (enter for 400)') || 400;
		var font_style = prompt('font style (enter for normal)') || 'normal';

		console.log(font_name, font_weight, font_style);
		embed_font(font_name, font_weight, font_style);
	});

	function embed_font (name, weight, style)
	{
		var file = font_picker_input.files[0];
		var reader = new FileReader();

		reader.onloadend = function ()
		{
			var data = reader.result.replace('data:;', 'data:font/woff;')

			var stylesheet = d.createElement('style');
			stylesheet.type = 'text/css';
			var css = "@font-face {font-family:'" + name + "';";
			css += "src: url(" + data + ") format('woff');"
			css += "font-weight:" + weight;
			css += ";font-style:" + style + "}";
			stylesheet.innerHTML = css;

			page.appendChild(stylesheet);
			console.log('font embedded!');
		}

		if (file)
		{
			reader.readAsDataURL(file);
		}
		else
		{
			console.log('something went wrong.');
		}
	}


	tools_panel.addEventListener('click', function (e)
	{
		e.stopPropagation();
		if (e.target.nodeName === 'BUTTON')
		{
			set_tool(e.target.textContent);
		}
	});



	// TODO: escape key should stop and cancel any dragging related event, including drawing a shape

	var document_name;
	// public API
	w.designer =
	{
		new_shape: new_shape,
		set_tool: set_tool,
		set_grid_size: set_grid_size,

		snap_to_grid: snap_to_grid,

		save_as: function (name)
		{
			var existing = localStorage.getItem(DOCUMENT_STORAGE_KEY_PREFIX + name);
			if (existing)
			{
				console.log('A document named ' + name + ' already exists. Choose another name or delete the existing document.');
			}
			else
			{
				localStorage.setItem(DOCUMENT_STORAGE_KEY_PREFIX + name, wrapper.innerHTML);
				document_name = name;
				console.log('Saved!');
			}
		},

		save: function (name)
		{
			if (document_name) // save existing document
			{
				localStorage.setItem(DOCUMENT_STORAGE_KEY_PREFIX + document_name, wrapper.innerHTML);
				console.log('Saved!');
			}
			else // this is a new document that hasn't been saved before
			{
				if (!name) name = prompt('Name for this document?');
				if (!name) return false;
				this.save_as(name);
			}
		},

		load: function (name)
		{
			var existing = localStorage.getItem(DOCUMENT_STORAGE_KEY_PREFIX + name);
			if (existing)
			{
				document_name = name;
				wrapper.innerHTML = existing;
				init_page();
			}
			else
			{
				console.log('Document not found!');
			}
		},

		delete: function (name)
		{
			localStorage.removeItem(DOCUMENT_STORAGE_KEY_PREFIX + name);
		}
	};

}(this, document);
